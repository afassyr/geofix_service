from django import forms
from django.contrib.auth import authenticate
from django.utils.text import capfirst
from django.utils.translation import gettext_lazy as _

from .models import User


# This form simply overwrites the default Django AuthenticationForm in order to
# replace 'username' field with 'email' field and to allow usage of widgets
# different than Django default which are not so great
class AuthenticationForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={
            "class": "form-control",
            "placeholder": "YOUR EMAIL"
        }
    ))
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control",
                "placeholder": "YOUR PASSWORD"
            }
        )
    )

    error_messages = {
        'invalid_login': _(
            "Please enter a correct %(email)s and password. Note that both "
            "fields may be case-sensitive."
        ),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

        # Set the max length and label for the "email" field.
        self.email_field = User._meta.get_field(User.USERNAME_FIELD)
        self.fields['email'].max_length = self.email_field.max_length or 254
        if self.fields['email'].label is None:
            self.fields['email'].label = capfirst(self.email_field.verbose_name)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email is not None and password:
            self.user_cache = authenticate(self.request, email=email,
                                           password=password)
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.
        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.
        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

    def get_invalid_login_error(self):
        return forms.ValidationError(
            self.error_messages['invalid_login'],
            code='invalid_login',
            params={'email': self.email_field.verbose_name},
        )


class UserRegistrationForm(forms.ModelForm):
    email = forms.EmailField(max_length=254,
                             widget=forms.EmailInput(attrs={
                                "class": "form-control",
                                "placeholder": "EMAIL",
                             }))
    first_name = forms.CharField(max_length=30, required=False,
                                 widget=forms.TextInput(attrs={
                                     "class": "form-control",
                                     "placeholder": "FIRST NAME",
                                 }))
    last_name = forms.CharField(max_length=30, required=False,
                                widget=forms.TextInput(attrs={
                                    "class": "form-control",
                                    "placeholder": "LAST NAME",
                                }))
    company = forms.CharField(max_length=30, required=False,
                              widget=forms.TextInput(attrs={
                                  "class": "form-control",
                                  "placeholder": "COMPANY",
                              }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
                                            "class": "form-control",
                                            "placeholder": "PASSWORD",
                                        }))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
                                            "class": "form-control",
                                            "placeholder": "REPEAT PASSWORD",
                                        }))

    class Meta:
        model = User
        fields = ("email", "company", "first_name", "last_name", "password",
                  "password2")

    def clean_password2(self):
        cd = self.cleaned_data
        if cd["password"] != cd["password2"]:
            raise forms.ValidationError("Passwords don\'t match.")
        return cd["password2"]
