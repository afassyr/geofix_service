#!/bin/sh

DB_FILE=db.sqlite3
BASE_DIR=/home/rafal/Projects/geofix_service/geofix_service
APPS=""
APPS="${APPS} account"
APPS="${APPS} upload"

for APP in ${APPS}
do
    if [ "$(ls -A '$APP/migrations' 2>/dev/null)" ]; then
        echo $?
        echo "$APP/migrations dir is empty. Moving on."
    else
        echo "Removing '$APP' app migrations."
        rm -rf $BASE_DIR/account/migrations/* $BASE_DIR/upload/migrations/* ./$DB_FILE
        if [ $? -eq 0 ]; then
            echo "Done."
        else
            echo "There's been some error. Check if '$APP' migration files and/or DB file have been removed."
        fi
    fi
done

echo "Preparing migrations..."
for APP in ${APPS}
do
    python manage.py makemigrations $APP 
done

echo "Migrating all..."
python manage.py migrate 
