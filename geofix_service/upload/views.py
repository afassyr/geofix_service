import configparser

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .forms import UploadFileForm


config = configparser.ConfigParser()
config.read("upload/upload.config")


def get_option_list(config, section, option):
    option_list = [item.strip() for item in config[section][option].split(",")]
    return option_list

sections = config.sections()
coordinates = get_option_list(config, "coordinates", "coordinates")
columns = get_option_list(config, "columns", "columns")
geoid_model = get_option_list(config, "geoid_model", "geoid_model")


@login_required
def upload_file(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return render(request, "upload/upload_success.html")
    else:
        form = UploadFileForm()
    return render(request, "upload/form.html", {"form": form,
                                                "sections": sections,
                                                "coordinates": coordinates,
                                                "columns": columns,
                                                "geoid_model": geoid_model})
