from django import forms
from upload.models import UploadedFile


class UploadFileForm(forms.ModelForm):
    uploaded_file = forms.FileField(widget=forms.ClearableFileInput(
        attrs={
            "class": "form-control-file",
        }
    ))

    class Meta:
        model = UploadedFile
        fields = ("uploaded_file",)
