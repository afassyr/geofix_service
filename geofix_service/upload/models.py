from __future__ import unicode_literals

from django.db import models


class UploadedFile(models.Model):
    uploaded_file = models.FileField(upload_to="user_uploads/")
