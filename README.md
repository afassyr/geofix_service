# README #

This is a small bundle of Django apps: 

* upload
* registration
* login

This project works with Python 3.6.1 and Django 1.11.3

Geofix website is meant to eventually host online RTK report generator for all major GNSS field software vendors, plus some utilities, like height conversion based on various quasigeoids models, vertical shift computation, stake out preview on OSM map, etc.